import fixQtImport as fix

fix._append_run_path()

__all__ = ["MyDelegate"]

from PyQt5.QtPrintSupport import QPrintDialog, QPrintPreviewDialog
from PyQt5.QtWidgets import QApplication, QDialog, QMainWindow, QHeaderView, QMessageBox, QDateEdit, QStyle, QFileDialog
from PyQt5 import QtWidgets, QtSql
from PyQt5.QtSql import QSqlRelationalDelegate
from PyQt5.QtCore import *
from PyQt5.QtGui import QPalette, QFontMetrics, QPdfWriter, QPageLayout, QPageSize, QTextDocument, QTextLength, QIcon

from mainForm import Ui_MainForm
from contractorsForm import Ui_contractorsForm
from kartaKontrForm import Ui_KartaKontrForm
from organizationsForm import Ui_organizationsForm
from realtyForm import Ui_realtyForm
from handbooksForm import Ui_handbooksForm
from kartaContractsForm import Ui_KartaContractsForm
from TextFormatHelper import *
import sqlite3, sys, os, traceback


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath('.'), relative_path)


def log_uncaught_exceptions(ex_cls, ex, tb):
    text = '{}: {}:\n'.format(ex_cls.__name__, ex)
    text += ''.join(traceback.format_tb(tb))

    print('Error: ', text)
    QMessageBox.critical(None, 'Error', text)
    quit()


sys.excepthook = log_uncaught_exceptions

conn = sqlite3.connect('db.db')
curs = conn.cursor()
curs.execute('CREATE TABLE IF NOT EXISTS contractors ('
             'ID INTEGER PRIMARY KEY AUTOINCREMENT,'
             'ORGANIZATION TEXT, '
             'CONTACT1 TEXT, '
             'CONTACT2 TEXT, '
             'CONTACT3 TEXT, '
             'EMAIL1 TEXT, '
             'EMAIL2 TEXT, '
             'EMAIL3 TEXT, '
             'TELEPHON1 TEXT,'
             'TELEPHON2 TEXT,'
             'TELEPHON3 TEXT)')
conn.commit()
curs.execute('CREATE TABLE IF NOT EXISTS organization ('
             'ID INTEGER PRIMARY KEY AUTOINCREMENT,'
             'ORGANIZATION TEXT)')
conn.commit()
curs.execute('CREATE TABLE IF NOT EXISTS realty ('
             'ID INTEGER PRIMARY KEY AUTOINCREMENT,'
             'REALTY TEXT)')
conn.commit()
curs.execute('CREATE TABLE IF NOT EXISTS contracts ('
             'ID INTEGER PRIMARY KEY AUTOINCREMENT,'
             'BEGIN_DATE DATE DEFAULT CURRENT_DATE,'
             'END_DATE DATE DEFAULT CURRENT_DATE,'
             'CONTR_ID INTEGER NOT NULL,'
             'ORGAN_ID INTEGER NOT NULL,'
             'REALTY_ID INTEGER NOT NULL,'
             'FOREIGN KEY (CONTR_ID) REFERENCES contractors(ID) ON DELETE CASCADE,'
             'FOREIGN KEY (ORGAN_ID) REFERENCES organization(ID) ON DELETE CASCADE,'
             'FOREIGN KEY (REALTY_ID) REFERENCES realty(ID) ON DELETE CASCADE)')
conn.commit()
curs.close()
conn.close()

db_connect = QtSql.QSqlDatabase.addDatabase("QSQLITE")
db_connect.setDatabaseName('db.db')
db_connect.open()


class MainTableModel(QtSql.QSqlRelationalTableModel):
    def __init__(self, parent=None, *args):
        super(MainTableModel, self).__init__()
        self.dateNow = QDate.currentDate()
        self.dateFuture = self.dateNow.addDays(14)
        self.dateKon = self.dateNow

    def data(self, index, role):
        col = index.column()
        row = index.row()

        if col == 2:
            d = QtSql.QSqlRelationalTableModel.data(self, index, QtCore.Qt.DisplayRole)
            strDate = "{y}-{m}-{day}".format(y=d[:4], m=d[5:7], day=d[8:10]) if d else d
            self.dateKon = QDate.fromString(strDate, 'yyyy-MM-dd')

            if (role == Qt.BackgroundRole) & (self.dateKon >= self.dateNow) & (self.dateKon <= self.dateFuture):
                bgColor = QColor(Qt.red)
                return QVariant(QColor(bgColor))
                # return QtGui.QColor(255, 0, 0)

        if role == Qt.BackgroundColorRole:
            if row % 2:
                bgColor = QColor(245, 247, 178)
            else:
                bgColor = QColor(222, 250, 175)

            return QVariant(QColor(bgColor))

        return super(MainTableModel, self).data(index, role)


class MyDelegate(QSqlRelationalDelegate):
    def __init__(self, parent=None):
        super(MyDelegate, self).__init__(parent)

    def paint(self, painter, option, index):
        painter.save()

        if not index.isValid():
            QSqlRelationalDelegate.paint(self, painter, option, index)

        if index.column() in [1, 2]:
            value = QDate.fromString(index.model().data(index, Qt.DisplayRole), Qt.ISODate)
            value = value.toString(Qt.SystemLocaleLongDate)

            align = Qt.AlignHCenter | Qt.AlignVCenter

            if option.state and QStyle.State_Active:

                if option.state & QStyle.State_Selected:
                    palette = QPalette.BrightText
                else:
                    palette = QPalette.WindowText
            else:
                palette = QPalette.WindowText

            self.drawBackground(painter, option, index)
            QApplication.style().drawItemText(painter, option.rect, align, option.palette, True, value, palette)
        else:
            QSqlRelationalDelegate.paint(self, painter, option, index)

        painter.restore()

    def createEditor(self, parent, option, index):
        # if index.column() in [4, 5]:
        #     return  # Read-only
        if index.column() in [1, 2]:
            editor = QDateEdit(parent)
            editor.setMinimumDate(QDate(2010, 1, 1))  # editor.setMinimumDate(QDate.currentDate())
            date = index.model().data(index, Qt.DisplayRole)

            if isinstance(date, QDate):
                cur_date = date
            elif isinstance(date, str):
                cur_date = QDate.fromString(date, 'yyyy-MM-dd')  # cur_date = QDate.currentDate()

            editor.setDate(cur_date)
            editor.setDisplayFormat("dd.MM.yyyy")
            editor.setCalendarPopup(True)
            editor.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
            return editor
        else:
            return QSqlRelationalDelegate.createEditor(self, parent, option, index)

    def setEditorData(self, editor, index):
        if index.column() in [1, 2]:
            date = index.model().data(index, Qt.DisplayRole)
            editor.setDate(QDate.fromString(date, 'dd.MM.yyyy'))
        else:
            QSqlRelationalDelegate.setEditorData(self, editor, index)

    def setModelData(self, editor, model, index):
        if not index.isValid():
            QSqlRelationalDelegate.setModelData(self, editor, model, index)
            return

        if index.column() in [1, 2]:
            date = editor.date().toString("yyyy-MM-dd")
            model.setData(index, date, Qt.EditRole)
        else:
            QSqlRelationalDelegate.setModelData(self, editor, model, index)

    def updateEditorGeometry(self, editor, option, index):
        if not index.isValid():
            QSqlRelationalDelegate.updateEditorGeometry(self, editor, option, index)

        if index.column() in [1, 2]:
            editor.setGeometry(option.rect)
        else:
            QSqlRelationalDelegate.updateEditorGeometry(self, editor, option, index)

    def sizeHint(self, option, index):
        size = QSqlRelationalDelegate.sizeHint(self, option, index)
        if index.isValid() and index.column() in [1, 2]:
            value = QDate.fromString(index.model().data(index, Qt.DisplayRole), Qt.ISODate)
            value = value.toString(Qt.SystemLocaleLongDate)
            fm = QFontMetrics(QApplication.font())

            return QSize(fm.width(value) + 5, size.height())
        else:
            return size


class MainApp(QMainWindow, Ui_MainForm):
    def __init__(self, parent=None):
        super(MainApp, self).__init__(parent)
        self.setupUi(self)
        self.Init_MainApp()
        self.LoadComboMain()
        self.timer = QTimer()
        self.timer.setInterval(600000)
        self.timer.timeout.connect(self.applyFilter)
        self.timer.start()

    def Init_MainApp(self):
        self.show()
        self.showMaximized()
        self.setWindowTitle('Программа для учета договоров с арендаторами')
        self.setWindowIcon(QIcon(resource_path('app.ico')))

        stm = MainTableModel(parent=self)
        stm.setEditStrategy(QtSql.QSqlTableModel.OnRowChange)
        stm.setTable("contracts")
        stm.setRelation(3, QtSql.QSqlRelation("contractors", "id", "organization"))
        stm.setRelation(4, QtSql.QSqlRelation("organization", "id", "organization"))
        stm.setRelation(5, QtSql.QSqlRelation("realty", "id", "realty"))
        stm.setSort(2, Qt.AscendingOrder)

        stm.setHeaderData(0, QtCore.Qt.Horizontal, "ID")
        stm.setHeaderData(1, QtCore.Qt.Horizontal, "Дата начала")
        stm.setHeaderData(2, QtCore.Qt.Horizontal, "Дата окончания")
        stm.setHeaderData(3, QtCore.Qt.Horizontal, "Контрагент")
        stm.setHeaderData(4, QtCore.Qt.Horizontal, "Наша Фирма")
        stm.setHeaderData(5, QtCore.Qt.Horizontal, "Объект недвижимости")
        stm.select()
        self.tableView.setModel(stm)
        self.tableView.setItemDelegate(MyDelegate(self))
        self.tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableView.selectRow(0)
        self.tableView.setFocus()

        if self.tableView.isColumnHidden(0) == False:
            self.tableView.setColumnHidden(0, True)

        header = self.tableView.horizontalHeader()

        stylesheet = "::section{Background-color:rgb(145, 250, 211)}"
        header.setStyleSheet(stylesheet)

        header.setSectionResizeMode(1, QHeaderView.Stretch)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QHeaderView.Stretch)

        self.toolButtonHandbooks.clicked.connect(self.Show_HandbooksForm)
        self.pBPrint.clicked.connect(self.handlePrint)
        self.pBPreviewPrint.clicked.connect(self.handlePreview)
        self.pBinPDF.clicked.connect(self.saveInPDF)
        self.pushButtonAdd.clicked.connect(self.Show_AddKartaContractsDialog)
        self.pushButtonDel.clicked.connect(self.KartaContractsDeleteRecord)
        self.pBClearKontr.clicked.connect(self.ClearComboKontr)
        self.pBClearRealty.clicked.connect(self.ClearComboRealty)
        self.chBOnlyRed.clicked.connect(self.applyFilter)
        self.cBSearchKontr.currentIndexChanged.connect(self.cBSearchKontrChanged)
        self.cBSearchRealty.currentIndexChanged.connect(self.cBSearchRealtyChanged)

        self.labelTotal.setText("Всего договоров: " + str(stm.rowCount()))

    def saveInPDF(self):
        fileName = QFileDialog.getSaveFileName(self, "Задайте имя файла", QDir.currentPath() + "/отчет.pdf",
                                               "pdf (* .pdf)")

        if fileName == None:
            print("please select a file")
            return

        myFile = QFile(fileName[0])

        if not myFile.open(QFile.WriteOnly):
            print("Ошибка открытия файла для записи!", fileName[0])
            return

        pdfWriter = QPdfWriter(myFile)
        pdfWriter.setPageLayout(QPageLayout(QPageSize(QPageSize.A4), QPageLayout.Portrait, QMarginsF(10, 10, 10, 10)))
        self.handlePaintRequest(pdfWriter)
        myFile.close()

    def handlePrint(self):
        dialog = QPrintDialog()

        if dialog.exec_() == QDialog.Accepted:
            self.handlePaintRequest(dialog.printer())

    def handlePreview(self):
        dialog = QPrintPreviewDialog()
        dialog.paintRequested.connect(self.handlePaintRequest)
        dialog.exec_()

    def handlePaintRequest(self, printer):

        formatCenter = blockFormatCenter()
        formatLeft = blockFormatLeft()
        formatLeftIndent = blockFormatLeftIndent()
        formatRight = blockFormatRight()

        normalFormat = charNormalFormat()
        headerFormat = charHeaderFormat()
        lineFormat = charLineFormat()
        boldunderFormat = charBoldUnderlinedFormat()
        totalFormat = charTotalFormat()
        footerFormat = charFooterFormat()

        # form = QTextCharFormat()
        # form.setBackground(Qt.red)

        curr_date = QDate.currentDate().toString("dd-MM-yyyy")

        document = QTextDocument()
        cursor = QTextCursor(document)

        cursor.beginEditBlock()
        cursor.setBlockFormat(formatLeft)
        cursor.insertText("Отчет из программы ""Договоры""", headerFormat)
        cursor.insertText("\n")
        cursor.insertText("на дату: " + curr_date, headerFormat)
        cursor.movePosition(QTextCursor.Down)
        cursor.endEditBlock()

        cursor.insertText("\n\n\n\n")

        # Table
        tableWidth = 6
        tablFormat = defaultTextTableFormat()
        tablFormat.setAlignment(QtCore.Qt.AlignLeft)
        # tablFormat.setLeftMargin(50)
        widths = [20, 120, 120, 120, 120, 120]
        widths = [QTextLength(QTextLength.FixedLength, width) for width in widths]
        tablFormat.setColumnWidthConstraints(widths)

        cursor.insertTable(1, tableWidth, tablFormat)
        insertCell(cursor, formatCenter, headerFormat, "№")
        insertCells(cursor, formatCenter, headerFormat,
                    ["Дата начала", "Дата окончания", "Контрагент", "Наша фирма", "Объект недвиж."])

        cursor.movePosition(QTextCursor.Down)

        model = self.tableView.model()
        table = cursor.insertTable(model.rowCount(), model.columnCount(), tablFormat)

        for row in range(table.rows()):
            for column in range(table.columns()):
                model = self.tableView.model()
                index = model.index(row, column)

                insText = str(model.data(index, Qt.DisplayRole))

                if column in [1, 2]:
                    insText = "{day}-{m}-{y}".format(y=insText[:4], m=insText[5:7],
                                                     day=insText[8:10]) if insText else insText

                if column == 0:
                    insText = str(row + 1)

                cursor.insertText(insText)
                cursor.movePosition(QTextCursor.NextCell)

        document.print_(printer)

    def OnlyRed(self):

        if self.chBOnlyRed.isChecked():
            dateNow = QDate.currentDate()
            dateFuture = dateNow.addDays(14)
            textSQLtext = "END_DATE BETWEEN '{}' AND '{}'".format(dateNow.toString('yyyy-MM-dd'),
                                                                  dateFuture.toString('yyyy-MM-dd'))
        else:
            textSQLtext = ''

        return textSQLtext

    def applyFilter(self):
        tAND = ''
        fAND = ''
        contrSQLtext = ''
        realtySQLtext = ''
        textSQLtext = self.OnlyRed()

        if textSQLtext != '':
            tAND = ' AND'
        else:
            fAND = ' AND'

        model = self.tableView.model()
        CONTR_ID = self.cBSearchKontr.itemData(self.cBSearchKontr.currentIndex())
        REALTY_ID = self.cBSearchRealty.itemData(self.cBSearchRealty.currentIndex())

        if REALTY_ID == None:
            REALTY_ID = 0

        if CONTR_ID != 0:
            contrSQLtext = tAND + ' CONTR_ID = {}'.format(CONTR_ID)

        if REALTY_ID != 0:
            realtySQLtext = tAND + ' REALTY_ID = {}'.format(REALTY_ID)

        if (CONTR_ID != 0) & (REALTY_ID != 0):
            textSQLtext = textSQLtext + contrSQLtext + fAND + realtySQLtext
        elif (CONTR_ID == 0) & (REALTY_ID != 0):
            textSQLtext = textSQLtext + realtySQLtext
        elif (CONTR_ID != 0) & (REALTY_ID == 0):
            textSQLtext = textSQLtext + contrSQLtext

        model.setFilter(textSQLtext)
        model.select()

    def cBSearchKontrChanged(self):
        self.applyFilter()

    def cBSearchRealtyChanged(self):
        self.applyFilter()

    def ClearComboKontr(self):
        self.cBSearchKontr.setCurrentIndex(0)

    def ClearComboRealty(self):
        self.cBSearchRealty.setCurrentIndex(0)

    def LoadComboMain(self):

        query = QtSql.QSqlQuery()
        query.exec("select ID, ORGANIZATION from contractors order by ORGANIZATION")

        # НАЧАЛО Заполним комбобоксы
        if query.isActive():
            self.cBSearchKontr.addItem('', 0)
            query.first()

            while query.isValid():
                self.cBSearchKontr.addItem(query.value('ORGANIZATION'), query.value('ID'))
                query.next()

        query.finish()

        query = QtSql.QSqlQuery()
        query.exec("select ID, REALTY from realty order by REALTY")

        if query.isActive():
            self.cBSearchRealty.addItem('', 0)
            query.first()

            while query.isValid():
                self.cBSearchRealty.addItem(query.value('REALTY'), query.value('ID'))
                query.next()
        query.finish()

    def Show_AddKartaContractsDialog(self):
        self.addContracts = kartaContractsDialog()
        self.Init_AddKartaContractorsDialog()
        self.addContracts.pushButtonSave.clicked.connect(self.AddDataContractsDialog)
        self.addContracts.pushButtonClose.clicked.connect(self.CloseAddDataContractsDialog)
        self.addContracts.exec_()

    def CloseAddDataContractsDialog(self):
        self.addContracts.close()

    def AddDataContractsDialog(self):
        rec = db_connect.record('contracts')
        rec.setValue('BEGIN_DATE', self.addContracts.NachDate.dateTime())
        rec.setValue('END_DATE', self.addContracts.KonDate.dateTime())
        rec.setValue('CONTR_ID',
                     int(self.addContracts.combKontra.itemData(self.addContracts.combKontra.currentIndex())))
        rec.setValue('ORGAN_ID', int(self.addContracts.combOrgan.itemData(self.addContracts.combOrgan.currentIndex())))
        rec.setValue('REALTY_ID',
                     int(self.addContracts.combRealty.itemData(self.addContracts.combRealty.currentIndex())))

        stm = self.tableView.model()
        stm.select()
        stm.insertRecord(-1, rec)
        stm.select()
        self.tableView.setModel(stm)
        self.labelTotal.setText("Всего договоров: " + str(stm.rowCount()))

    def Init_AddKartaContractorsDialog(self):
        self.DefaultSetDates()

        query = QtSql.QSqlQuery()
        query.exec("select ID, ORGANIZATION from contractors order by ORGANIZATION")

        # НАЧАЛО Заполним комбобоксы
        if query.isActive():
            query.first()
            while query.isValid():
                self.addContracts.combKontra.addItem(query.value('ORGANIZATION'), query.value('ID'))
                query.next()

        query.finish()

        query = QtSql.QSqlQuery()
        query.exec("select ID, ORGANIZATION from organization order by ORGANIZATION")

        if query.isActive():
            query.first()
            while query.isValid():
                self.addContracts.combOrgan.addItem(query.value('ORGANIZATION'), query.value('ID'))
                query.next()

        query.finish()

        query = QtSql.QSqlQuery()
        query.exec("select ID, REALTY from realty order by REALTY")

        if query.isActive():
            query.first()
            while query.isValid():
                self.addContracts.combRealty.addItem(query.value('REALTY'), query.value('ID'))
                query.next()

    # КОНЕЦ Заполним комбобоксы

    def DefaultSetDates(self):
        currentDate = QtCore.QDateTime.currentDateTime()
        Nach_dt = self.addContracts.NachDate.dateTime().toString(self.addContracts.NachDate.displayFormat())
        Kon_dt = self.addContracts.KonDate.dateTime().toString(self.addContracts.KonDate.displayFormat())

        if Nach_dt == '01.01.2000':
            self.addContracts.NachDate.setDateTime(currentDate)
        if Kon_dt == '01.01.2000':
            self.addContracts.KonDate.setDateTime(currentDate)

    def KartaContractsDeleteRecord(self):
        res = QMessageBox.warning(self, "Текущая запись будет НАВСЕГДА удалена!!!",
                                  "Подумай еще раз и нажми No. Продолжить?",
                                  buttons=QMessageBox.Yes | QMessageBox.No,
                                  defaultButton=QMessageBox.No)

        if res == QMessageBox.Yes:
            index_row = self.tableView.currentIndex().row()
            stm = self.tableView.model()
            stm.removeRow(index_row)
            stm.select()
            self.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    # НАЧАЛО Форма списка справочников
    def Show_HandbooksForm(self):
        self.handbooks = handbooksForm()
        self.Init_HandbooksForm()
        self.handbooks.exec_()

    def Init_HandbooksForm(self):
        self.handbooks.setWindowTitle('Справочники')
        self.handbooks.pushButtonKontr.clicked.connect(self.Show_ContractorsForm)
        self.handbooks.pushButtonOrgans.clicked.connect(self.Show_OrganizationsForm)
        self.handbooks.pushButtonRealty.clicked.connect(self.Show_RealtyForm)

    # КОНЕЦ Форма списка справочников

    # НАЧАЛО Форма списка объектов недвижимости
    def Show_RealtyForm(self):
        self.handbooks.close()
        self.realty = realtyForm()
        self.Init_RealtyForm()
        self.realty.exec_()

    def Init_RealtyForm(self):
        self.realty.setWindowTitle('Справочник объектов недвижимости')
        stm = QtSql.QSqlTableModel(parent=self)
        stm.setTable('realty')
        stm.setHeaderData(0, QtCore.Qt.Horizontal, "ID")
        stm.setHeaderData(1, QtCore.Qt.Horizontal, "Объект недвижимости")
        stm.select()
        self.realty.tableView.setModel(stm)
        self.realty.tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        if self.realty.tableView.isColumnHidden(0) == False:
            self.realty.tableView.setColumnHidden(0, True)

        header = self.realty.tableView.horizontalHeader()
        header.setSectionResizeMode(1, QHeaderView.Stretch)

        self.realty.pushButtonAdd.clicked.connect(self.AddRealtyRecord)
        self.realty.pushButtonDel.clicked.connect(self.DeleteRealtyRecord)
        self.realty.pushButtonClose.clicked.connect(self.CloseRealtyDialog)
        self.realty.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def AddRealtyRecord(self):
        stm = self.realty.tableView.model()
        rec = db_connect.record('realty')
        rec.setValue('realty', 'Новый объект')
        stm.insertRecord(-1, rec)
        stm.submit()
        self.realty.tableView.setModel(stm)
        stm.select()
        self.realty.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def DeleteRealtyRecord(self):
        res = QMessageBox.warning(self, "Текущая запись будет НАВСЕГДА удалена!!!",
                                  "Подумай еще раз и нажми No. Продолжить?",
                                  buttons=QMessageBox.Yes | QMessageBox.No,
                                  defaultButton=QMessageBox.No)

        if res == QMessageBox.Yes:
            index_row = self.realty.tableView.currentIndex().row()
            stm = self.realty.tableView.model()
            stm.removeRow(index_row)
            stm.select()
            self.realty.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def CloseRealtyDialog(self):
        self.realty.close()

    # КОНЕЦ Форма списка объектов недвижимости

    # НАЧАЛО Форма списка Наши Фирмы
    def Show_OrganizationsForm(self):
        self.handbooks.close()
        self.organ = organizationsForm()
        self.Init_OrganizationsForm()
        self.organ.exec_()

    def Init_OrganizationsForm(self):
        self.organ.setWindowTitle('Справочник наших фирм')
        stm = QtSql.QSqlTableModel(parent=self)
        stm.setTable('organization')
        stm.setHeaderData(0, QtCore.Qt.Horizontal, "ID")
        stm.setHeaderData(1, QtCore.Qt.Horizontal, "Организация")
        stm.select()
        self.organ.tableView.setModel(stm)
        self.organ.tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        if self.organ.tableView.isColumnHidden(0) == False:
            self.organ.tableView.setColumnHidden(0, True)

        header = self.organ.tableView.horizontalHeader()
        header.setSectionResizeMode(1, QHeaderView.Stretch)

        self.organ.pushButtonAdd.clicked.connect(self.AddOrganRecord)
        self.organ.pushButtonDel.clicked.connect(self.DeleteOrganRecord)
        self.organ.pushButtonClose.clicked.connect(self.CloseOrganDialog)
        self.organ.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def AddOrganRecord(self):
        stm = self.organ.tableView.model()
        rec = db_connect.record('organization')
        rec.setValue('organization', 'Новая фирма')
        stm.insertRecord(-1, rec)
        stm.submit()
        self.organ.tableView.setModel(stm)
        stm.select()
        self.organ.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def DeleteOrganRecord(self):
        res = QMessageBox.warning(self, "Текущая запись будет НАВСЕГДА удалена!!!",
                                  "Подумай еще раз и нажми No. Продолжить?",
                                  buttons=QMessageBox.Yes | QMessageBox.No,
                                  defaultButton=QMessageBox.No)

        if res == QMessageBox.Yes:
            index_row = self.organ.tableView.currentIndex().row()
            stm = self.organ.tableView.model()
            stm.removeRow(index_row)
            stm.select()
            self.organ.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def CloseOrganDialog(self):
        self.organ.close()

    # КОНЕЦ Форма списка Наши Фирмы

    # НАЧАЛО Форма списка контрагентов
    def Show_ContractorsForm(self):
        self.handbooks.close()
        self.contr = contractorsForm()
        self.Init_ContractorsForm()
        self.contr.exec_()

    def Init_ContractorsForm(self):
        self.contr.setWindowTitle('Справочник контрагентов')
        stm = QtSql.QSqlTableModel(parent=self)
        stm.setTable('contractors')

        stm.setHeaderData(1, QtCore.Qt.Horizontal, "Организация")
        stm.setHeaderData(2, QtCore.Qt.Horizontal, "Контактное лицо")
        stm.setHeaderData(5, QtCore.Qt.Horizontal, "E-Mail")
        stm.setHeaderData(8, QtCore.Qt.Horizontal, "Телефон")

        stm.select()
        self.contr.tableView.setModel(stm)

        if self.contr.tableView.isColumnHidden(0) == False:
            self.contr.tableView.setColumnHidden(0, True)
        if self.contr.tableView.isColumnHidden(3) == False:
            self.contr.tableView.setColumnHidden(3, True)
        if self.contr.tableView.isColumnHidden(4) == False:
            self.contr.tableView.setColumnHidden(4, True)
        if self.contr.tableView.isColumnHidden(6) == False:
            self.contr.tableView.setColumnHidden(6, True)
        if self.contr.tableView.isColumnHidden(7) == False:
            self.contr.tableView.setColumnHidden(7, True)
        if self.contr.tableView.isColumnHidden(9) == False:
            self.contr.tableView.setColumnHidden(9, True)
        if self.contr.tableView.isColumnHidden(10) == False:
            self.contr.tableView.setColumnHidden(10, True)

        self.contr.tableView.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.contr.tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        header = self.contr.tableView.horizontalHeader()
        header.setSectionResizeMode(1, QHeaderView.Stretch)

        self.contr.pushButtonAdd.clicked.connect(self.Show_AddKontraDialog)
        self.contr.pushButtonEdit.clicked.connect(self.Show_EditKontraDialog)
        self.contr.pushButtonDel.clicked.connect(self.DeleteKontra)
        self.contr.pushButtonClose.clicked.connect(self.CloseContractorsForm)
        self.contr.tableView.doubleClicked.connect(self.Show_EditKontraDialog)
        self.contr.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def CloseContractorsForm(self):
        self.contr.close()

    def DeleteKontra(self):
        res = QMessageBox.warning(self, "Текущая запись будет НАВСЕГДА удалена!!!",
                                  "Подумай еще раз и нажми No. Продолжить?",
                                  buttons=QMessageBox.Yes | QMessageBox.No,
                                  defaultButton=QMessageBox.No)

        if res == QMessageBox.Yes:
            index_row = self.contr.tableView.currentIndex().row()
            stm = self.contr.tableView.model()
            stm.removeRow(index_row)
            stm.select()
            self.contr.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    # КОНЕЦ Форма списка контрагентов

    # НАЧАЛО Форма диалога добавления контрагента
    def Show_AddKontraDialog(self):
        self.addKontra = addKontraDialog()
        self.addKontra.pushButtonClose.clicked.connect(self.CloseAddKontraDialog)
        self.addKontra.pushButtonSave.clicked.connect(self.AddKontraDialog)
        self.addKontra.exec_()

    def AddKontraDialog(self):
        rec = db_connect.record('contractors')
        rec.setValue('ORGANIZATION', self.addKontra.lineEditOrgan.text())
        rec.setValue('CONTACT1', self.addKontra.lineEditLico1.text())
        rec.setValue('CONTACT2', self.addKontra.lineEditLico2.text())
        rec.setValue('CONTACT3', self.addKontra.lineEditLico3.text())
        rec.setValue('EMAIL1', self.addKontra.lineEditEmail1.text())
        rec.setValue('EMAIL2', self.addKontra.lineEditEmail2.text())
        rec.setValue('EMAIL3', self.addKontra.lineEditEmail3.text())
        rec.setValue('TELEPHON1', self.addKontra.lineEditTele1.text())
        rec.setValue('TELEPHON2', self.addKontra.lineEditTele2.text())
        rec.setValue('TELEPHON3', self.addKontra.lineEditTele3.text())

        stm = self.contr.tableView.model()
        stm.insertRecord(-1, rec)
        stm.submit()
        self.contr.tableView.setModel(stm)
        stm.select()

        self.addKontra.lineEditOrgan.clear()
        self.addKontra.lineEditLico1.clear()
        self.addKontra.lineEditLico2.clear()
        self.addKontra.lineEditLico3.clear()
        self.addKontra.lineEditEmail1.clear()
        self.addKontra.lineEditEmail2.clear()
        self.addKontra.lineEditEmail3.clear()
        self.addKontra.lineEditTele1.clear()
        self.addKontra.lineEditTele2.clear()
        self.addKontra.lineEditTele3.clear()
        self.contr.labelTotal.setText("Всего записей: " + str(stm.rowCount()))

    def CloseAddKontraDialog(self):
        self.addKontra.close()

    # КОНЕЦ Форма диалога добавления контрагента

    # НАЧАЛО Форма диалога редактирования контрагента
    def Show_EditKontraDialog(self):
        self.editKontra = addKontraDialog()

        index_row = self.contr.tableView.currentIndex().row()
        rec = self.contr.tableView.model().record(index_row)

        self.editKontra.lineEditOrgan.setText(rec.value('ORGANIZATION'))
        self.editKontra.lineEditLico1.setText(rec.value('CONTACT1'))
        self.editKontra.lineEditLico2.setText(rec.value('CONTACT2'))
        self.editKontra.lineEditLico3.setText(rec.value('CONTACT3'))
        self.editKontra.lineEditEmail1.setText(rec.value('EMAIL1'))
        self.editKontra.lineEditEmail2.setText(rec.value('EMAIL2'))
        self.editKontra.lineEditEmail3.setText(rec.value('EMAIL3'))
        self.editKontra.lineEditTele1.setText(rec.value('TELEPHON1'))
        self.editKontra.lineEditTele2.setText(rec.value('TELEPHON2'))
        self.editKontra.lineEditTele3.setText(rec.value('TELEPHON3'))

        self.editKontra.pushButtonSave.clicked.connect(self.EditKontraDialog)
        self.editKontra.pushButtonClose.clicked.connect(self.CloseEditKontraDialog)
        self.editKontra.exec_()
        self.contr.labelTotal.setText("Всего записей: " + str(self.contr.tableView.model().rowCount()))

    def EditKontraDialog(self):
        index_row = self.contr.tableView.currentIndex().row()
        rec = self.contr.tableView.model().record(index_row)
        rec.setValue('ORGANIZATION', self.editKontra.lineEditOrgan.text())
        rec.setValue('CONTACT1', self.editKontra.lineEditLico1.text())
        rec.setValue('CONTACT2', self.editKontra.lineEditLico2.text())
        rec.setValue('CONTACT3', self.editKontra.lineEditLico3.text())
        rec.setValue('EMAIL1', self.editKontra.lineEditEmail1.text())
        rec.setValue('EMAIL2', self.editKontra.lineEditEmail2.text())
        rec.setValue('EMAIL3', self.editKontra.lineEditEmail3.text())
        rec.setValue('TELEPHON1', self.editKontra.lineEditTele1.text())
        rec.setValue('TELEPHON2', self.editKontra.lineEditTele2.text())
        rec.setValue('TELEPHON3', self.editKontra.lineEditTele3.text())

        stm = self.contr.tableView.model()
        stm.setRecord(index_row, rec)
        stm.select()
        self.contr.tableView.setModel(stm)
        self.editKontra.close()

    def CloseEditKontraDialog(self):
        self.editKontra.close()
    # КОНЕЦ Форма диалога редактирования контрагента


class kartaContractsDialog(QDialog, Ui_KartaContractsForm):
    def __init__(self, parent=None):
        super(kartaContractsDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('app.ico')))


class handbooksForm(QDialog, Ui_handbooksForm):
    def __init__(self, parent=None):
        super(handbooksForm, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('app.ico')))


class realtyForm(QDialog, Ui_realtyForm):
    def __init__(self, parent=None):
        super(realtyForm, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('app.ico')))


class organizationsForm(QDialog, Ui_organizationsForm):
    def __init__(self, parent=None):
        super(organizationsForm, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('app.ico')))


class contractorsForm(QDialog, Ui_contractorsForm):
    def __init__(self, parent=None):
        super(contractorsForm, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('app.ico')))


class addKontraDialog(QDialog, Ui_KartaKontrForm):
    def __init__(self, parent=None):
        super(addKontraDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('app.ico')))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = MainApp()
    sys.exit(app.exec_())
