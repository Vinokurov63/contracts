# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainForm.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainForm(object):
    def setupUi(self, MainForm):
        MainForm.setObjectName("MainForm")
        MainForm.resize(904, 600)
        MainForm.setMinimumSize(QtCore.QSize(800, 600))
        MainForm.setStyleSheet("QLineEdit{\n"
"background-color: rgb(0, 0, 0);\n"
"}\n"
"QLineEdit{\n"
"color: rgb(0, 255, 0);\n"
"}\n"
"QLineEdit:Hover{\n"
"background-color: rgb(255, 255, 0);\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainForm)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(28)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.horizontalLayout.addWidget(self.line_2)
        self.horizontalLayout_4.addLayout(self.horizontalLayout)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.toolButtonHandbooks = QtWidgets.QToolButton(self.centralwidget)
        self.toolButtonHandbooks.setMinimumSize(QtCore.QSize(100, 30))
        self.toolButtonHandbooks.setObjectName("toolButtonHandbooks")
        self.horizontalLayout_2.addWidget(self.toolButtonHandbooks)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pBPreviewPrint = QtWidgets.QPushButton(self.centralwidget)
        self.pBPreviewPrint.setMinimumSize(QtCore.QSize(100, 30))
        self.pBPreviewPrint.setMaximumSize(QtCore.QSize(200, 16777215))
        self.pBPreviewPrint.setObjectName("pBPreviewPrint")
        self.horizontalLayout_2.addWidget(self.pBPreviewPrint)
        self.pBPrint = QtWidgets.QPushButton(self.centralwidget)
        self.pBPrint.setMinimumSize(QtCore.QSize(100, 30))
        self.pBPrint.setMaximumSize(QtCore.QSize(200, 16777215))
        self.pBPrint.setObjectName("pBPrint")
        self.horizontalLayout_2.addWidget(self.pBPrint)
        self.pBinPDF = QtWidgets.QPushButton(self.centralwidget)
        self.pBinPDF.setMinimumSize(QtCore.QSize(100, 30))
        self.pBinPDF.setMaximumSize(QtCore.QSize(200, 16777215))
        self.pBinPDF.setObjectName("pBinPDF")
        self.horizontalLayout_2.addWidget(self.pBinPDF)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.pushButtonAdd = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonAdd.setMinimumSize(QtCore.QSize(100, 30))
        self.pushButtonAdd.setMaximumSize(QtCore.QSize(200, 16777215))
        self.pushButtonAdd.setObjectName("pushButtonAdd")
        self.horizontalLayout_2.addWidget(self.pushButtonAdd)
        self.pushButtonDel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonDel.setMinimumSize(QtCore.QSize(100, 30))
        self.pushButtonDel.setMaximumSize(QtCore.QSize(200, 16777215))
        self.pushButtonDel.setObjectName("pushButtonDel")
        self.horizontalLayout_2.addWidget(self.pushButtonDel)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout.addWidget(self.line_3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.chBOnlyRed = QtWidgets.QCheckBox(self.centralwidget)
        self.chBOnlyRed.setObjectName("chBOnlyRed")
        self.horizontalLayout_3.addWidget(self.chBOnlyRed)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem3)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.cBSearchKontr = QtWidgets.QComboBox(self.centralwidget)
        self.cBSearchKontr.setMinimumSize(QtCore.QSize(150, 0))
        self.cBSearchKontr.setObjectName("cBSearchKontr")
        self.horizontalLayout_3.addWidget(self.cBSearchKontr)
        self.pBClearKontr = QtWidgets.QPushButton(self.centralwidget)
        self.pBClearKontr.setMinimumSize(QtCore.QSize(20, 0))
        self.pBClearKontr.setMaximumSize(QtCore.QSize(20, 16777215))
        self.pBClearKontr.setObjectName("pBClearKontr")
        self.horizontalLayout_3.addWidget(self.pBClearKontr)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem4)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.cBSearchRealty = QtWidgets.QComboBox(self.centralwidget)
        self.cBSearchRealty.setMinimumSize(QtCore.QSize(150, 0))
        self.cBSearchRealty.setObjectName("cBSearchRealty")
        self.horizontalLayout_3.addWidget(self.cBSearchRealty)
        self.pBClearRealty = QtWidgets.QPushButton(self.centralwidget)
        self.pBClearRealty.setMinimumSize(QtCore.QSize(20, 0))
        self.pBClearRealty.setMaximumSize(QtCore.QSize(20, 16777215))
        self.pBClearRealty.setObjectName("pBClearRealty")
        self.horizontalLayout_3.addWidget(self.pBClearRealty)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem5)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4.addLayout(self.verticalLayout)
        self.gridLayout_2.addLayout(self.horizontalLayout_4, 0, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout_2.addWidget(self.line, 1, 0, 1, 1)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.tableView = QtWidgets.QTableView(self.frame)
        self.tableView.setShowGrid(True)
        self.tableView.setGridStyle(QtCore.Qt.DashLine)
        self.tableView.setSortingEnabled(True)
        self.tableView.setObjectName("tableView")
        self.tableView.horizontalHeader().setCascadingSectionResizes(False)
        self.tableView.horizontalHeader().setDefaultSectionSize(200)
        self.gridLayout.addWidget(self.tableView, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.frame, 2, 0, 1, 1)
        self.labelTotal = QtWidgets.QLabel(self.centralwidget)
        self.labelTotal.setObjectName("labelTotal")
        self.gridLayout_2.addWidget(self.labelTotal, 3, 0, 1, 1)
        MainForm.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainForm)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 904, 21))
        self.menubar.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.menubar.setObjectName("menubar")
        MainForm.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainForm)
        self.statusbar.setObjectName("statusbar")
        MainForm.setStatusBar(self.statusbar)

        self.retranslateUi(MainForm)
        QtCore.QMetaObject.connectSlotsByName(MainForm)

    def retranslateUi(self, MainForm):
        _translate = QtCore.QCoreApplication.translate
        MainForm.setWindowTitle(_translate("MainForm", "MainWindow"))
        self.label.setText(_translate("MainForm", "Договоры"))
        self.toolButtonHandbooks.setText(_translate("MainForm", "Справочники"))
        self.pBPreviewPrint.setText(_translate("MainForm", "Предпросмотр"))
        self.pBPrint.setText(_translate("MainForm", "Печатать"))
        self.pBinPDF.setText(_translate("MainForm", "в PDF"))
        self.pushButtonAdd.setText(_translate("MainForm", "Добавить"))
        self.pushButtonDel.setText(_translate("MainForm", "Удалить"))
        self.chBOnlyRed.setText(_translate("MainForm", "Только красные"))
        self.label_3.setText(_translate("MainForm", "Контрагент:"))
        self.pBClearKontr.setText(_translate("MainForm", "X"))
        self.label_4.setText(_translate("MainForm", "Объект:"))
        self.pBClearRealty.setText(_translate("MainForm", "X"))
        self.labelTotal.setText(_translate("MainForm", "Total users:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainForm = QtWidgets.QMainWindow()
    ui = Ui_MainForm()
    ui.setupUi(MainForm)
    MainForm.show()
    sys.exit(app.exec_())
