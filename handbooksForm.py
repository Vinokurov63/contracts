# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'handbooksForm.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_handbooksForm(object):
    def setupUi(self, handbooksForm):
        handbooksForm.setObjectName("handbooksForm")
        handbooksForm.resize(170, 140)
        handbooksForm.setMinimumSize(QtCore.QSize(170, 140))
        handbooksForm.setMaximumSize(QtCore.QSize(170, 140))
        self.gridLayout = QtWidgets.QGridLayout(handbooksForm)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(handbooksForm)
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.line = QtWidgets.QFrame(handbooksForm)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.pushButtonKontr = QtWidgets.QPushButton(handbooksForm)
        self.pushButtonKontr.setObjectName("pushButtonKontr")
        self.verticalLayout.addWidget(self.pushButtonKontr)
        self.pushButtonOrgans = QtWidgets.QPushButton(handbooksForm)
        self.pushButtonOrgans.setObjectName("pushButtonOrgans")
        self.verticalLayout.addWidget(self.pushButtonOrgans)
        self.pushButtonRealty = QtWidgets.QPushButton(handbooksForm)
        self.pushButtonRealty.setObjectName("pushButtonRealty")
        self.verticalLayout.addWidget(self.pushButtonRealty)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(handbooksForm)
        QtCore.QMetaObject.connectSlotsByName(handbooksForm)

    def retranslateUi(self, handbooksForm):
        _translate = QtCore.QCoreApplication.translate
        handbooksForm.setWindowTitle(_translate("handbooksForm", "Dialog"))
        self.label.setText(_translate("handbooksForm", "Справочники"))
        self.pushButtonKontr.setText(_translate("handbooksForm", "Контрагенты"))
        self.pushButtonOrgans.setText(_translate("handbooksForm", "Наши Фирмы"))
        self.pushButtonRealty.setText(_translate("handbooksForm", "Объекты недвижимости"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    handbooksForm = QtWidgets.QDialog()
    ui = Ui_handbooksForm()
    ui.setupUi(handbooksForm)
    handbooksForm.show()
    sys.exit(app.exec_())
