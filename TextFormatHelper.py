from decimal import Decimal

from PyQt5 import QtCore
from PyQt5.QtGui import QTextTableFormat, QTextFrameFormat, QTextBlockFormat, QTextCharFormat, QTextCursor, \
    QTextFormat, QColor

# from common.functions import moneyToStr


# Block formats
def blockFormatCenter():
    blockFormat = QTextBlockFormat()
    blockFormat.setAlignment(QtCore.Qt.AlignHCenter)
    return blockFormat


def blockFormatRight():
    blockFormat = QTextBlockFormat()
    blockFormat.setAlignment(QtCore.Qt.AlignRight)
    return blockFormat


def blockFormatLeft():
    blockFormat = QTextBlockFormat()
    blockFormat.setAlignment(QtCore.Qt.AlignLeft)
    return blockFormat


def blockFormatLeftIndent():
    blockFormat = blockFormatLeft()
    blockFormat.setIndent(1.5)
    return blockFormat


def blockFormatPageBreak():
    blockFormat = QTextBlockFormat()
    blockFormat.setPageBreakPolicy(QTextFormat.PageBreak_AlwaysAfter)
    return blockFormat


# Char formats
def charHeaderFormat():
    headerFormat = QTextCharFormat()
    headerFormat.setFontWeight(35)
    # headerFormat.setFontUnderline(True)
    headerFormat.setFontPointSize(12)
    return headerFormat


def charLineFormat():
    lineFormat = QTextCharFormat()
    lineFormat.setFontPointSize(12)
    return lineFormat


def charNormalFormat():
    normalFormat = QTextCharFormat()
    normalFormat.setFontWeight(50)
    normalFormat.setFontPointSize(12)
    return normalFormat


def charBoldUnderlinedFormat():
    boldUnderlined = QTextCharFormat()
    boldUnderlined.setFontPointSize(12)
    boldUnderlined.setFontWeight(75)
    boldUnderlined.setFontUnderline(True)
    return boldUnderlined


def charTotalFormat():
    totalFormat = charBoldUnderlinedFormat()
    totalFormat.setFontOverline(True)
    return totalFormat


def charFooterFormat():
    footerFormat = QTextCharFormat()
    footerFormat.setFontPointSize(13)
    footerFormat.setFontWeight(75)
    return footerFormat


# Text table format
def defaultTextTableFormat():
    # A QTextTableFormat with the defaults we like
    tblFormat = QTextTableFormat()
    tblFormat.setBorderStyle(QTextFrameFormat.BorderStyle_Outset)
    tblFormat.setCellSpacing(0)
    tblFormat.setCellPadding(0)
    tblFormat.setAlignment(QtCore.Qt.AlignHCenter)
    return tblFormat


# Insertions
def insertCells(cursor: QTextCursor, blockFormat: QTextBlockFormat, charFormat: QTextCharFormat, texts):
    for text in texts:
        insertCell(cursor, blockFormat, charFormat, text)


def insertCell(cursor: QTextCursor, blockFormat: QTextBlockFormat, charFormat: QTextCharFormat, text):
    cursor.setBlockFormat(blockFormat)
    cursor.insertText(str(text), charFormat)
    cursor.movePosition(QTextCursor.NextCell)


# def insertMoneyCell(cursor: QTextCursor, blockFormat: QTextBlockFormat, charFormat: QTextCharFormat, value: Decimal):
#     if value < 0.00:
#         charFormat = QTextCharFormat(charFormat)
#         charFormat.setForeground(QColor(QtCore.Qt.red))
#     insertCell(cursor, blockFormat, charFormat, moneyToStr(value))